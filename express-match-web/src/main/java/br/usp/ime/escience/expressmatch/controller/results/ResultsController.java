package br.usp.ime.escience.expressmatch.controller.results;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.usp.ime.escience.expressmatch.service.results.MatchResultVo;
import br.usp.ime.escience.expressmatch.service.results.ResultInstance;
import br.usp.ime.escience.expressmatch.service.results.ResultsServiceProvider;


@Component
@Scope("session")
public class ResultsController implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ResultsServiceProvider provider;
	
	public static final String AREA_CHART = "area_chart";
	public static final String LINE_CHART = "line_chart";
	public static final String BAR_CHART = "bar_chart";

	private String selectedChart;
	
	private LineChartModel lineChartModel;
	
	private LineChartModel areaChartModel;
	
	private BarChartModel barChartModel;
	
	private List<ChartSeries> chartSeries;

	private DualListModel<Integer> typesDualList;
	
	private DualListModel<String> seriesDualList;
	
	private List<String> series;
	
	private List<String> availableSeries;
	
	private List<Integer> modelExpressions;
	
	private List<Integer> availableModelExpressions;
	
	private List<MatchResultVo> results;
	
	@PostConstruct
	public void init(){
		FacesContext.getCurrentInstance().getExternalContext().getSession(true);
		initSeries();
	}


	public void updateCharts() {
		loadSeries();
		loadLineChart();
		loadBarChart();
	}
	
	
	private void loadSeries() {
		
		this.chartSeries = new ArrayList<ChartSeries>();
		
		
		for (MatchResultVo matchResultVo : results) {
			
			if (this.series.contains(matchResultVo.getName())) {
				ChartSeries series = new ChartSeries();
		        series.setLabel(matchResultVo.getName());
		        
		        for (ResultInstance instance : matchResultVo.getResultInstances()) {
			        if (modelExpressions.contains(instance.getExpressionType().getId())) {
			        	series.set(instance.getExpressionType().getId(), instance.getResult());
			        }
		        }
		        this.chartSeries.add(series);
			}
		}
		
	}


	private void initSeries() {
		this.results = provider.getMatchResults();
		this.availableModelExpressions = new ArrayList<Integer>();
		this.modelExpressions = new ArrayList<Integer>();
		this.series = new ArrayList<String>();
		this.availableSeries = new ArrayList<String>();
		

		int i = 0;
		for (MatchResultVo matchResultVo : results) {
			this.availableSeries.add(matchResultVo.getName());
	        for (ResultInstance instance : matchResultVo.getResultInstances()) {
		        if (i == 0) {
		        	availableModelExpressions.add(instance.getExpressionType().getId());
		        }
	        }
	        i++;
		}
		Collections.sort(this.availableModelExpressions);

		
		seriesDualList = new DualListModel<String>(availableSeries, series);
		typesDualList = new DualListModel<Integer>(availableModelExpressions, modelExpressions);
	}


	private void loadBarChart() {


        BarChartModel model = new BarChartModel();
 
       for (ChartSeries chartSerie : chartSeries) {
    	   model.addSeries(chartSerie);
       }

       model.setTitle("Expression Match Results");
       model.setLegendPosition("ne");
       model.setShowPointLabels(true);

       Axis xAxis = model.getAxis(AxisType.X);
       xAxis.setLabel("Expression model id");
        
       Axis yAxis = model.getAxis(AxisType.Y);
       yAxis.setLabel("Correct matching rate"); 
       yAxis.setMin(0);
       yAxis.setMax(1);

        this.barChartModel = model;
	}

	private void loadLineChart() {

        LineChartModel model = new LineChartModel();
 
        for (ChartSeries chartSerie : chartSeries) {
     	   model.addSeries(chartSerie);
        }

        model.setTitle("Expression Match Results");
        model.setLegendPosition("ne");
        model.setShowPointLabels(true);

        Axis xAxis = model.getAxis(AxisType.X);
        xAxis.setLabel("Expression model id");
         
        Axis yAxis = model.getAxis(AxisType.Y);
        yAxis.setLabel("Correct matching rate"); 
        yAxis.setMin(0);
        yAxis.setMax(1);
        
        this.lineChartModel = model;	
	}
	
	public void onTypeTransfer(TransferEvent event) {
		
		if (event.isAdd()) {
			for(Object item : event.getItems()) {
	        	this.modelExpressions.add(Integer.valueOf((String)item));
				for (Iterator<Integer> iterator = this.availableModelExpressions.iterator(); iterator.hasNext();) {
					Integer model = iterator.next();
					if (model.equals(Integer.valueOf((String)item))) {
						iterator.remove();
						break;
					}
				}
	        }
		} else if (event.isRemove()) {
			for(Object item : event.getItems()) {
	        	this.availableModelExpressions.add(Integer.valueOf((String)item));
				for (Iterator<Integer> iterator = this.modelExpressions.iterator(); iterator.hasNext();) {
					Integer model = iterator.next();
					if (model.equals(Integer.valueOf((String)item))) {
						iterator.remove();
						break;
					}
				}
			}
		}
		Collections.sort(this.modelExpressions);
		Collections.sort(this.availableModelExpressions);
		typesDualList = new DualListModel<Integer>(availableModelExpressions, modelExpressions);
	}
	
	public void onSeriesTransfer(TransferEvent event) {
		
		if (event.isAdd()) {
			for(Object item : event.getItems()) {
	        	this.series.add((String)item);
				for (Iterator<String> iterator = this.availableSeries.iterator(); iterator.hasNext();) {
					String model = iterator.next();
					if (model.equals((String)item)) {
						iterator.remove();
						break;
					}
				}
	        }
		} else if (event.isRemove()) {
			for(Object item : event.getItems()) {
	        	this.availableSeries.add((String)item);
				for (Iterator<String> iterator = this.series.iterator(); iterator.hasNext();) {
					String model = iterator.next();
					if (model.equals((String)item)) {
						iterator.remove();
						break;
					}
				}
			}
		}
		Collections.sort(this.series);
		Collections.sort(this.availableSeries);
		seriesDualList = new DualListModel<String>(availableSeries, series);
        
	}
	
	public void onChartTypeChange(AjaxBehaviorEvent e){
		this.selectedChart = (String) ((SelectOneMenu)e.getSource()).getValue();
	}

	public ResultsServiceProvider getProvider() {
		return provider;
	}

	public void setProvider(ResultsServiceProvider provider) {
		this.provider = provider;
	}

	public String getSelectedChart() {
		return selectedChart;
	}

	public void setSelectedChart(String selectedChart) {
		this.selectedChart = selectedChart;
	}

	public CartesianChartModel getLineChartModel() {
		return lineChartModel;
	}

	public void setLineChartModel(LineChartModel lineChartModel) {
		this.lineChartModel = lineChartModel;
	}

	public LineChartModel getAreaChartModel() {
		return areaChartModel;
	}

	public void setAreaChartModel(LineChartModel areaChartModel) {
		this.areaChartModel = areaChartModel;
	}

	public BarChartModel getBarChartModel() {
		return barChartModel;
	}

	public void setBarChartModel(BarChartModel barChartModel) {
		this.barChartModel = barChartModel;
	}


	public List<Integer> getModelExpressions() {
		return modelExpressions;
	}


	public void setModelExpressions(List<Integer> modelExpressions) {
		this.modelExpressions = modelExpressions;
	}


	public List<Integer> getAvailableModelExpressions() {
		return availableModelExpressions;
	}


	public void setAvailableModelExpressions(List<Integer> availableModelExpressions) {
		this.availableModelExpressions = availableModelExpressions;
	}



	public List<MatchResultVo> getResults() {
		return results;
	}


	public void setResults(List<MatchResultVo> results) {
		this.results = results;
	}


	public List<ChartSeries> getChartSeries() {
		return chartSeries;
	}


	public void setChartSeries(List<ChartSeries> chartSeries) {
		this.chartSeries = chartSeries;
	}


	public DualListModel<Integer> getTypesDualList() {
		return typesDualList;
	}


	public void setTypesDualList(DualListModel<Integer> typesDualList) {
		this.typesDualList = typesDualList;
	}


	public DualListModel<String> getSeriesDualList() {
		return seriesDualList;
	}


	public void setSeriesDualList(DualListModel<String> seriesDualList) {
		this.seriesDualList = seriesDualList;
	}


	public List<String> getSeries() {
		return series;
	}


	public void setSeries(List<String> series) {
		this.series = series;
	}


	public List<String> getAvailableSeries() {
		return availableSeries;
	}


	public void setAvailableSeries(List<String> availableSeries) {
		this.availableSeries = availableSeries;
	}
	
}
