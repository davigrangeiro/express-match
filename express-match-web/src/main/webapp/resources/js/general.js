/**
 * 
 */

function showDiv(id) {
	var element = document.getElementById(id);
	element.classList.remove('hidden');
}

function hideDiv(id) {
	var element = document.getElementById(id);
	element.classList.add('hidden');
}