package br.usp.ime.escience.expressmatch.service.graph.cost;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.usp.ime.escience.expressmatch.model.Expression;
import br.usp.ime.escience.expressmatch.model.Point;
import br.usp.ime.escience.expressmatch.model.Stroke;
import br.usp.ime.escience.expressmatch.model.Symbol;
import br.usp.ime.escience.expressmatch.model.UserParameter;
import br.usp.ime.escience.expressmatch.model.graph.Graph;
import br.usp.ime.escience.expressmatch.service.symbol.match.preprocessing.PreprocessingAlgorithms;
import br.usp.ime.escience.expressmatch.service.user.UserServiceProvider;


@Service
@Transactional
public class ShapeContextServiceProvider {


	private static final Logger logger = LoggerFactory.getLogger(ShapeContextServiceProvider.class);
	
	@Autowired
	private UserServiceProvider userServiceProvider;
	
	
//	public double[][] evaluateSymbolShapeContext(Symbol s, UserParameter parameters){
//
//		s = PreprocessingAlgorithms.preprocessSymbol(s);
//		Point[] points = PreprocessingAlgorithms.getNPoints(s, parameters.getPointsPerSymbol());
//		
//		Graph g = new Graph();
//        ShapeContext sc;
//        
//        for (int i = 0; i < points.length; i++) {
//            g.addVertex(i, (float)points[i].getX(), (float)points[i].getY());
//        }
//            
//        float diagonal = (float)Math.sqrt(Math.pow(g.getHeight(), 2) + Math.pow(g.getWidth(), 2));
//        
//        sc = new ShapeContext(diagonal, g, parameters.getPolarLocalRegions(), parameters.getAngularLocalRegions(), false);
//        
//        return sc.getSC();
//	}
//	
//	public double[][] evaluateStrokeShapeContextForExpression(Expression expression, UserParameter parameters){
//
//		Graph g = new Graph();
//        ShapeContext sc;
//        
//        int i = 0;
//        for (Symbol symbol : expression.getSymbols()) {
//			for (Stroke stroke : symbol.getStrokes()) {
//				Point strokeRepresentantPoint = stroke.getRepresentantPointOfStroke();
//				
//				g.addVertex(i, stroke.getStrokeId(), strokeRepresentantPoint.getX(), strokeRepresentantPoint.getY());
//				i = i+1;
//			}
//		}
//                    
//        float diagonal = (float)Math.sqrt(Math.pow(g.getHeight(), 2) + Math.pow(g.getWidth(), 2));
//        
//        sc = new ShapeContext(diagonal, g, parameters.getPolarGlobalRegions(), parameters.getAngularGlobalRegions(), false);
//        
//        return sc.getSC();
//	}
//
//	
//	

//	public double[][] getShapeContextDescriptor(Symbol s){
//		double[][] res = null;
//		
//    	UserParameter parameters = this.userServiceProvider.getUserParameters();
//    	res = this.evaluateSymbolShapeContext(s, parameters);
//	    
//		return res;
//	}
//
//	public double[][] getShapeContextDescriptor(List<Stroke>  strokes){
//		double[][] res = null;
//		
//		Symbol s = new Symbol();
//		for (Stroke stroke: strokes) {
//			Stroke newStroke = new Stroke();
//			for (Point p : stroke.getPoints()) {
//				newStroke.addCheckingBoundingBox(p);
//			}
//			s.addCheckingBoundingBox(newStroke);
//		}
//		
//    	UserParameter parameters = this.userServiceProvider.getUserParameters();
//    	res = this.evaluateSymbolShapeContext(s, parameters);
//	    
//		return res;
//	}	
	
	
	
}