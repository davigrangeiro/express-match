package br.usp.ime.escience.expressmatch.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import br.usp.ime.escience.expressmatch.service.match.evaluate.memo.Cacheable;

@Entity
@Table(name = "simulated_expression_match", catalog = "expressMatch")
public class SimulatedExpressionMatch implements Serializable, Cacheable{

	private static final long serialVersionUID = 1L;
	
	private Long id;
	private ExpressionMatch match;
	private Integer modelSize;
	private Integer correctMatchedSize;
	private Date insertDate;
	
	public SimulatedExpressionMatch() {
		super();
	}

	@Id
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@OneToOne(fetch = FetchType.LAZY, cascade=CascadeType.PERSIST)
	@PrimaryKeyJoinColumn
	public ExpressionMatch getMatch() {
		return match;
	}

	public void setMatch(ExpressionMatch match) {
		this.match = match;
	}

	@Column(name = "model_size", nullable = false)
	public Integer getModelSize() {
		return modelSize;
	}

	public void setModelSize(Integer modelSize) {
		this.modelSize = modelSize;
	}

	@Column(name = "correct_matched_size", nullable = false)
	public Integer getCorrectMatchedSize() {
		return correctMatchedSize;
	}

	public void setCorrectMatchedSize(Integer correctMatchedSize) {
		this.correctMatchedSize = correctMatchedSize;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "insert_date")
	public Date getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((correctMatchedSize == null) ? 0 : correctMatchedSize
						.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((insertDate == null) ? 0 : insertDate.hashCode());
		result = prime * result
				+ ((modelSize == null) ? 0 : modelSize.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimulatedExpressionMatch other = (SimulatedExpressionMatch) obj;
		if (correctMatchedSize == null) {
			if (other.correctMatchedSize != null)
				return false;
		} else if (!correctMatchedSize.equals(other.correctMatchedSize))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (insertDate == null) {
			if (other.insertDate != null)
				return false;
		} else if (!insertDate.equals(other.insertDate))
			return false;
		if (modelSize == null) {
			if (other.modelSize != null)
				return false;
		} else if (!modelSize.equals(other.modelSize))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SimulatedExpressionMatch [id=" + id + ", modelSize="
				+ modelSize + ", correctMatchedSize=" + correctMatchedSize
				+ "]";
	}

	@Override
	@Transient
	public String getStringKey() {
		return ""+ this.getMatch().getMatchRequest().getId();
	}
			
}
