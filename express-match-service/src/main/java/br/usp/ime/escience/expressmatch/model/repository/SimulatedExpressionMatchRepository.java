package br.usp.ime.escience.expressmatch.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.usp.ime.escience.expressmatch.model.SimulatedExpressionMatch;

public interface SimulatedExpressionMatchRepository extends JpaRepository<SimulatedExpressionMatch, Long> {

}
