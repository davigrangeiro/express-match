package br.usp.ime.escience.expressmatch.constants;

public class SystemConstants {

	public static final int SYMBOL_DESCRIPTOR_TYPE = 1; 
	public static final int EXPRESSION_STROKE_CENTROID_DESCRIPTOR_TYPE = 2; 
	public static final float MATCH_DEFAULT_VALUE = 2000000.f;
	
}
