package br.usp.ime.escience.expressmatch.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.usp.ime.escience.expressmatch.model.MatchRequest;

public interface MatchRequestRepository extends JpaRepository<MatchRequest, Long> {

}
