package br.usp.ime.escience.expressmatch.service.match;

public class SymbolDistance implements Comparable<SymbolDistance>{

	private String id;
	private Double distance;
	
	@Override
	public int compareTo(SymbolDistance o) {
		return this.distance.compareTo(o.getDistance());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public SymbolDistance() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SymbolDistance(String id, double distance) {
		super();
		this.id = id;
		this.distance = distance;
	}
	
	
}
