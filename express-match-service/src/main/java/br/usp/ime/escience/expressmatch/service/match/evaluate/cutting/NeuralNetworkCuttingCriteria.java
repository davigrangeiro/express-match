package br.usp.ime.escience.expressmatch.service.match.evaluate.cutting;

import br.usp.ime.escience.expressmatch.service.match.evaluate.SymbolMatchHypothesis;

public class NeuralNetworkCuttingCriteria implements CuttingCriteria{

	private static final double NEURAL_NETWORK_CUTTING_THRESHOLD = .999;

	@Override
	public boolean isToCutHypothesis(SymbolMatchHypothesis hypothesis) {
		return hypothesis.cost > NEURAL_NETWORK_CUTTING_THRESHOLD;
	}

}
