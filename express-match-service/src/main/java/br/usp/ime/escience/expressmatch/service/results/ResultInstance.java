package br.usp.ime.escience.expressmatch.service.results;

import java.io.Serializable;

import br.usp.ime.escience.expressmatch.model.ExpressionType;

public class ResultInstance implements Serializable{

	private static final long serialVersionUID = 1L;

	private ExpressionType expressionType;
	private Float result;
	
	public ExpressionType getExpressionType() {
		return expressionType;
	}
	public void setExpressionType(ExpressionType expressionType) {
		this.expressionType = expressionType;
	}
	public Float getResult() {
		return result;
	}
	public void setResult(Float result) {
		this.result = result;
	}
	
}
