package br.usp.ime.escience.expressmatch.service.symbol.classifier;

import java.util.Arrays;

import br.usp.ime.escience.expressmatch.model.Symbol;

public class SymbolClassifierResponse {

	private float symbolCost;
	private float agregatedCost;
	private Symbol usedSymbol;
	private int[] permutation;
	

	public float getSymbolCost() {
		return symbolCost;
	}

	public void setSymbolCost(float cost) {
		this.symbolCost = cost;
	}

	public int[] getPermutation() {
		return permutation;
	}

	public void setPermutation(int[] permutation) {
		this.permutation = permutation;
	}

	public Symbol getUsedSymbol() {
		return usedSymbol;
	}

	public void setUsedSymbol(Symbol usedSymbol) {
		this.usedSymbol = usedSymbol;
	}

	@Override
	public String toString() {
		return "SymbolClassifierResponse [symbol cost=" + symbolCost + ", agregated cost=" + agregatedCost + ", usedSymbol="
				+ usedSymbol.getHref() + ", permutation=" + Arrays.toString(permutation)
				+ "]";
	}

	public float getAgregatedCost() {
		return agregatedCost;
	}

	public void setAgregatedCost(float agregatedCost) {
		this.agregatedCost = agregatedCost;
	}
	
	public boolean isIdInPermutation(int id) {
		boolean res = false;
		
		for (int i : permutation) {
			if (id == i) {
				res = true;
				break;
			}
		}
		
		return res;
	}
	
}
