package br.usp.ime.escience.expressmatch.service.symbol.classifier.neural.net;

import br.usp.ime.vision.frank.classifier.NeuralNetworkClassifier;

public class NeuralNetworkClassifierSingleton {

	private static NeuralNetworkClassifier classifier;
	
	private NeuralNetworkClassifierSingleton() {
		super();
	}
	
	public static NeuralNetworkClassifier getInstance() {
		if (null == classifier) {
			classifier = NeuralNetworkClassifier.newDefaultInstance();
		}
		return classifier;
	}
}
