package br.usp.ime.escience.expressmatch.service.match;

public class StrokeDistance implements Comparable<StrokeDistance>{

	private int id;
	private Double distance;
	
	@Override
	public int compareTo(StrokeDistance o) {
		return this.distance.compareTo(o.getDistance());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public StrokeDistance() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StrokeDistance(int id, double distance) {
		super();
		this.id = id;
		this.distance = distance;
	}
	
	
}
