package br.usp.ime.escience.expressmatch.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.usp.ime.escience.expressmatch.model.ExpressionMatch;

public interface ExpressionMatchRepository extends JpaRepository<ExpressionMatch, Long> {

}
