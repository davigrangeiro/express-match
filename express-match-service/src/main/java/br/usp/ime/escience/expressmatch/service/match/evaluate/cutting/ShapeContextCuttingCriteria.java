package br.usp.ime.escience.expressmatch.service.match.evaluate.cutting;

import br.usp.ime.escience.expressmatch.service.match.evaluate.SymbolMatchHypothesis;

public class ShapeContextCuttingCriteria implements CuttingCriteria {

	private static final float SHAPE_CONTEXT_CUTTING_THRESHOLD = .5f;

	@Override
	public boolean isToCutHypothesis(SymbolMatchHypothesis hypothesis) {
		boolean res = false;
		if (hypothesis.cost > SHAPE_CONTEXT_CUTTING_THRESHOLD) {
			res = true;
		}
		
		return res;
	}

}
