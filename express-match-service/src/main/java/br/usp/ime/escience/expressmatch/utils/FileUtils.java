package br.usp.ime.escience.expressmatch.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class FileUtils {

	
	public static BufferedReader getBufferedReaderFromFile(File in) throws FileNotFoundException {
		FileReader fileReader = new FileReader(in);
		BufferedReader reader = new BufferedReader(fileReader);
		return reader;
	}
	
	
}
