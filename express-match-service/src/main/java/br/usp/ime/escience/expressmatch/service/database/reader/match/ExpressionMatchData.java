package br.usp.ime.escience.expressmatch.service.database.reader.match;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ExpressionMatchData implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer type;
	private String user;
	private Map<Integer, Integer> symbolMatch;
	
	public ExpressionMatchData(Integer type, String user) {
		super();
		this.type = type;
		this.user = user;
		this.symbolMatch = new HashMap<Integer, Integer>();
	}

	public ExpressionMatchData() {
		super();
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Map<Integer, Integer> getSymbolMatch() {
		return symbolMatch;
	}

	public void setSymbolMatch(Map<Integer, Integer> symbolMatch) {
		this.symbolMatch = symbolMatch;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExpressionMatchData other = (ExpressionMatchData) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}
	
	
}
