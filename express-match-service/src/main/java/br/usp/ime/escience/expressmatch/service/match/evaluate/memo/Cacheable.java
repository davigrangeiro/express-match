package br.usp.ime.escience.expressmatch.service.match.evaluate.memo;

public interface Cacheable {

	String getStringKey();
	
}
