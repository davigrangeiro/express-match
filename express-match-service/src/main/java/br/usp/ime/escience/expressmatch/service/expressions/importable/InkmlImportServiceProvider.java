package br.usp.ime.escience.expressmatch.service.expressions.importable;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.usp.ime.escience.expressmatch.model.Expression;
import br.usp.ime.escience.expressmatch.model.ExpressionType;
import br.usp.ime.escience.expressmatch.model.Symbol;
import br.usp.ime.escience.expressmatch.service.database.reader.inkml.EMGTDomReader;
import br.usp.ime.escience.expressmatch.service.database.reader.match.ExpressionMatchData;
import br.usp.ime.escience.expressmatch.service.database.reader.match.ExpressionMatchDatabaseReader;
import br.usp.ime.escience.expressmatch.service.expressions.ExpressionServiceProvider;
import br.usp.ime.escience.expressmatch.service.graph.cost.ShapeContextServiceProvider;
import br.usp.ime.escience.expressmatch.service.user.UserServiceProvider;

@Service
@Transactional
public class InkmlImportServiceProvider  implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(InkmlImportServiceProvider.class);
	
	private final String ALL = "ALL";

	private final String FILES_PREFIX[] = {this.ALL};

	private static final String ROOT_NICK = "root";

	private static final String ROOT_USER = "user0";

	@Autowired
	private ExpressionServiceProvider expressionProvider;

	@Autowired
	private EMGTDomReader inkmlDomReader;
	
	@Autowired
	private UserServiceProvider userServiceProvider;
	
	@Autowired
	private ShapeContextServiceProvider shapeContextServiceProvider;
	
	@Async
	public void importDataSet(){
		LOGGER.info("Importing dataset");
		
		try {
		
			List<Expression> rootExpressions = new ArrayList<Expression>();
			List<Expression> expressions = inkmlDomReader.getDataSet(FILES_PREFIX);
			
			
			for (Expression expression : expressions) {
					
				if(null != expression){
			
					if(ROOT_USER.equals(expression.getUserInfo().getName())){
						
						LOGGER.info(MessageFormat.format("Found a model Expression: {0}", expression.getExpressionType().getId()));
						
						expression.getUserInfo().setName(ROOT_NICK);
						rootExpressions.add(expression);
					}
					
					expression.setUserInfo(this.userServiceProvider.loadUserInformation(expression.getUserInfo().getName()));
				}		
			}

			List<ExpressionType> models = expressionProvider.generateExpressionTypesByExpression(rootExpressions);
			
			LIST_OF_EXPRESSIONS: for (Expression expression : expressions) {
				for (ExpressionType rootExpression : models) {
					if(expression.getExpressionTransientId().intValue() == rootExpression.getId()){
						expression.setExpressionType(rootExpression);
						
						if(ROOT_NICK.equalsIgnoreCase(expression.getUserInfo().getUser().getNick())){
							rootExpression.setExpression(expression);
						}
							
						continue LIST_OF_EXPRESSIONS;
					}
				}
			}
			
			expressions = adjustExpressions(expressions);
			
			expressionProvider.saveExpressions(expressions);
			expressionProvider.saveExpressionTypes(models);
			
		} catch (Exception e) {
			LOGGER.error("There was an error while importing dataset", e);
		}
	}

	private List<Expression> adjustExpressions(List<Expression> expressions) {
		LOGGER.info("Starting adjusting expressions");
		
		ExpressionMatchDatabaseReader reader = new ExpressionMatchDatabaseReader();
		Map<ExpressionMatchData, ExpressionMatchData> map = reader.getExpressionMatchData();
		ExpressionMatchData key = new ExpressionMatchData();
		
		for (Expression expression : expressions) {
			
			key.setType(expression.getExpressionType().getId());
			key.setUser(expression.getUserInfo().getUser().getNick().toLowerCase());
					
			try {
				
				if(!ROOT_USER.equals(key.getUser())) {
					expression = adjustExpression(expression, map.get(key));
				}
				
			} catch (Exception e) {
				LOGGER.error(MessageFormat.format("Error while adjusting expression {0} for user {1}", expression.getLabel(), expression.getUserInfo().getName()));
			}
		}
		
		LOGGER.info("Finishing adjusting expressions");
		return expressions;
	}

	private Expression adjustExpression(Expression expression, ExpressionMatchData expressionMatchData) throws Exception{
		
		Expression modelExpression = expression.getExpressionType().getExpression();
		
		Map<Integer, Symbol> modelMap = getMapForSymbol(modelExpression);
		Map<Integer, Symbol> expressionMap = getMapForSymbol(expression);
		
		Symbol model = null, currentSymbol = null;
		for (Entry<Integer, Integer> entry : expressionMatchData.getSymbolMatch().entrySet()) {
			
			model = modelMap.get(entry.getKey());
			currentSymbol = expressionMap.get(entry.getValue());
			
			currentSymbol.setHref(model.getHref());
		}
		
		LOGGER.info(MessageFormat.format("Expression {0} for user {1} adjusted", expression.getLabel(), expression.getUserInfo().getName()));
		
		return expression;
		
	}

	private Map<Integer, Symbol> getMapForSymbol(Expression expression) {
		Map<Integer, Symbol> map = new HashMap<Integer, Symbol>();
		
		for (Symbol s : expression.getSymbols()) {
			map.put(s.getSymbolId(), s);
		}
		
		return map;
	}
	
}