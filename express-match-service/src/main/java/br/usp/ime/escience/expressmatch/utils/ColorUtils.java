package br.usp.ime.escience.expressmatch.utils;

public class ColorUtils {

	private static String[] letters = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
	
	public static String getRandomColor() {
	    String color = "#";
	    for (int i = 0; i < 6; i++ ) {
	        color += letters[Double.valueOf(Math.random() * 16.0).intValue()];
	    }
	    return color;
	}
	
}
